    Cacti spreadsheet.  Contains three sheets:

    CACTI data for regular caches.
    CACTI data for victim caches.
    AAT calculations for the validation runs 0 through 7.

    The table below shows which cache configurations do not have cacti data, the reason why, and the graphs that are impacted w.r.t. missing datapoints corresponding to these cache configurations.

    Cache configurations without cacti data.
    SIZE
    	BLOCKSIZE
    	ASSOC
    	Reason
    	Graphs Impacted
    1024 	32
    	8 	fewer than 8 sets (4)
    	 #2,#3 (missing one AAT datapoint)
    1024
    	64
    	4
    	fewer than 8 sets (4)
    	none; #4 doesn't plot AAT
    1024
    	128
    	4
    	fewer than 8 sets (2)
    	none; #4 doesn't plot AAT
    2048
    	128
    	4
    	fewer than 8 sets (4)
    	none; #4 doesn't plot AAT
