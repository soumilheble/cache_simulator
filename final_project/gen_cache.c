/**
 * \file gen_cache.c
 * \author Soumil Krishnanand Heble
 * \date 09/27/2018
 * \brief Source: General cache implementation with optional victim cache
 */

#include "gen_cache.h"
#include <stdlib.h> /** C Standard library header file for dynamic memory allocation functions */

/**
 * \brief Clear dynamically allocated memory for the cache and its associated structures
 * \param[in] mem_to_free Pointer to the cache construct whose memory is to be freed
 */
void free_alloc_mem(cache_core *mem_to_free)
{
    /** If VC is enabled clear it first */
    if(mem_to_free->cache_vc_blks)
    {
        free((mem_to_free->my_vcache)->vc_cache_mem);   /** Clear the cache blocks allocated to VC */
        free(mem_to_free->my_vcache);                   /** Clear the memory allocated for the VC */
    }
    
    free(mem_to_free->cache_mem);   /** Clear the cache blocks allocated to this cache instance */
    free(mem_to_free);              /** Clear this cache instance */
}

/**
 * \brief Allocate memory for cache data memory
 * \param[in] cache_sets Number of sets in the cache
 * \param[in] cache_assoc Associativity of the cache
 * \return Pointer to the instantiated cache memory block of type cache_entry
 */
cache_entry *init_cache_memblk(unsigned int cache_sets, unsigned int cache_assoc)
{
    cache_entry *cache_entry_temp = (cache_entry *)malloc(sizeof(cache_entry)*cache_sets*cache_assoc);
    
    if(cache_entry_temp==NULL)
    {
        exit(3);    /** Fault: If malloc unable to allocate cache block memory exit with error #3 */
    }
    
    clear_cache_blks(cache_entry_temp, cache_sets, cache_assoc);
    return cache_entry_temp;
}

/**
 * \brief Clear and initialize cache data memory
 * \param[in] blks_to_clr Pointer to the cache memory block of type cache_entry
 * \param[in] cache_setno Number of sets in the cache
 * \param[in] cache_assocno Associativity of the cache
 */
void clear_cache_blks(cache_entry *blks_to_clr, unsigned int cache_setno, unsigned int cache_assocno)
{
    unsigned int temp_sets;
    unsigned int temp_ways;
    for(temp_sets=0; temp_sets<cache_setno; temp_sets++)
    {
        for(temp_ways=0; temp_ways<cache_assocno; temp_ways++)
        {
            (blks_to_clr+(temp_sets*cache_assocno)+temp_ways)->cache_index = temp_sets;
            (blks_to_clr+(temp_sets*cache_assocno)+temp_ways)->cache_tag = temp_ways;
            (blks_to_clr+(temp_sets*cache_assocno)+temp_ways)->block_valid = 0;
            (blks_to_clr+(temp_sets*cache_assocno)+temp_ways)->block_dirty = 0;
            (blks_to_clr+(temp_sets*cache_assocno)+temp_ways)->repp_rank = cache_assocno;
        }
    }
}

/**
 * \brief Allocate memory for victim cache and initialize it
 * \param[in] vc_blks Number of blocks in the victim cache
 * \param[in] tag_bmask Bit mask for Tag extraction
 * \return Pointer to the instantiated victim cache of type vcache_core
 */
vcache_core *init_vcache(unsigned int vc_blks, unsigned int tag_bmask, unsigned char tag_bpos)
{
    vcache_core *vcache_core_temp = (vcache_core *)malloc(sizeof(vcache_core));
    
    if(vcache_core_temp==NULL)
    {
        exit(2);    /** Fault: If malloc unable to allocate vc memory exit with error #2 */
    }
    
    /** Set Cache Configuration Variables */
    vcache_core_temp->vcache_blks = vc_blks;
    vcache_core_temp->tag_bitmask = tag_bmask;
    vcache_core_temp->tag_bitpos = tag_bpos;
    
    /** Clear Cache Counter Variables */
    vcache_core_temp->cache_swap_success = 0;
    vcache_core_temp->cache_wback = 0;
    
    /** Set Memory Structure Pointers */
    vcache_core_temp->vc_cache_mem = init_cache_memblk((unsigned int)1, vcache_core_temp->vcache_blks);
    
    return vcache_core_temp;
}

/**
 * \brief Allocate memory for a cache instance and initialize it
 * \param[in] cache_lvl Cache level in memory hierarchy
 * \param[in] size Cache size in bytes
 * \param[in] blk_size Cache block size in bytes
 * \param[in] assoc Cache associativity
 * \param[in] vc_blocks Number of blocks in victim cache
 * \return Pointer to the instantiated cache of type cache_core
 */
cache_core *init_cache_level(unsigned char cache_lvl, unsigned int size, unsigned int blk_size, unsigned int assoc, unsigned int vc_blocks)
{
    cache_core *cache_core_temp = (cache_core *)malloc(sizeof(cache_core));
    
    if(cache_core_temp==NULL)
    {
        exit(1);    /** Fault: If malloc unable to allocate main cache memory exit with error #1 */
    }
    
    /** Set Cache Configuration Variables */
    cache_core_temp->cache_level = cache_lvl;
    cache_core_temp->cache_size = size;
    cache_core_temp->cache_assoc = assoc;
    cache_core_temp->cache_blksize = blk_size;
    cache_core_temp->cache_vc_blks = vc_blocks;
    
    /** Set Cache Derived Variables */
    cache_core_temp->cache_nsets = size/(blk_size*assoc);   /** Number of sets = cache_size/(block_size + cache_associativity) */
    
    cache_core_temp->blkoffs_bitmask = blk_size-1;  /** Block offset bitmask = block_size-1 (E.g. if block_size = 8. block offset bitmask = 8-1 = 7 = 0b111) */
    
    /** Count number of bits set in the block offset bitmask */
    unsigned int no_of_bits = 0;
    unsigned int temp = cache_core_temp->blkoffs_bitmask;
    
    while(temp&1)  /** Number of bits = right shift operations until the mask is zero */
    {
        no_of_bits++;
        temp >>= 1;
    }
    
    cache_core_temp->ind_bitpos = no_of_bits;   /** Index bits position = number of bits set in block offset bit mask. */
    cache_core_temp->ind_bitmask = (cache_core_temp->cache_nsets-1)<<(cache_core_temp->ind_bitpos); /** Index bitmask = ((index_size = number_of_sets)-1) << position_of_index_bits_in_address. (E.g. number_of_sets = 64 and position_of_index_bits_in_address = 3, number_of_sets-1 = 64-1 = 63 = 0b111111, index_bitmask = 0b111111<<3 = 0b111111000) */
    
    /** Count number of bits set in the index bitmask */
    no_of_bits = 0;
    temp = cache_core_temp->cache_nsets-1;    /** Number of bits = right shift operations until the mask is zero */
    while(temp&1)
    {
        no_of_bits++;
        temp >>= 1;
    }
    
    cache_core_temp->tag_bitpos = no_of_bits + (cache_core_temp->ind_bitpos);   /** Tag bits position = number of bits set in index bit mask + number of bits set in block offset bit mask */
    
    cache_core_temp->tag_bitmask = ~((cache_core_temp->blkoffs_bitmask)|(cache_core_temp->ind_bitmask));  /** Tag bitmask = max(unsigned int = 32-bit unsigned variable in gcc compiler) & (block_offset_bitmask | index_bitmask). (E.g. block_offset_bitmask = 0b111 and index_bitmask = 0b111111000, tag_bitmask = 0xFFFFFFFF&0b111111111 = 0xFFFFFE00) */
    
    /** Clear Cache Counter Variables */
    cache_core_temp->cacheacc_read = 0;
    cache_core_temp->cacheacc_rmiss = 0;
    cache_core_temp->cacheacc_write = 0;
    cache_core_temp->cacheacc_wmiss = 0;
    
    /** Allocate/Initialize appropriate variables depending on whether VC is enabled or not */
    if(cache_core_temp->cache_vc_blks)
    {
        cache_core_temp->cache_wback = 0xFFFFFFFF;
        cache_core_temp->cache_swapreq = 0;
        cache_core_temp->my_vcache = init_vcache(cache_core_temp->cache_vc_blks, ~(cache_core_temp->blkoffs_bitmask), cache_core_temp->ind_bitpos); /** VC tag = tag + index of parent cache thus the tag bitmask = ~block offset bit mask */ 
    }
    else
    {
        cache_core_temp->cache_wback = 0;
        cache_core_temp->cache_swapreq = 0xFFFFFFFF;
        cache_core_temp->my_vcache = NULL;
    }
    
    /** Set Memory Structure Pointers */
    cache_core_temp->cache_mem = init_cache_memblk(cache_core_temp->cache_nsets, cache_core_temp->cache_assoc);
    
    cache_core_temp->next_lvl_mem = NULL;
    
    return cache_core_temp;
}
