/**
 * \file gen_cache.h
 * \author Soumil Krishnanand Heble
 * \date 09/27/2018
 * \brief Header: General cache implementation with optional victim cache
 */

#ifndef __GEN_CACHE_H__
#define __GEN_CACHE_H__

/** \brief Structure of each block in the cache data memory */
typedef struct cache_entry
{
    unsigned int cache_index;   /**< Cache index tag */ //Redundant
    unsigned int cache_tag;     /**< Cache entry tag */
    unsigned char block_valid;  /**< Block valid flag */
    unsigned char block_dirty;  /**< Block dirty flag */
    unsigned int repp_rank;    /**< Block replacement policy rank */
} cache_entry;

/** \brief Structure of the victim cache (VC) */
typedef struct vcache_core
{        
    /** Cache Configuration Variables */
    unsigned int vcache_blks;           /**< Number of blocks in vcache*/
    unsigned int tag_bitmask;           /**< Tag bitmask */
    unsigned char tag_bitpos;           /**< Position of the tag bits in address */
    
    /** Cache Counter Variables */
    unsigned int cache_swap_success;    /**< Cache swap success counter */
    unsigned int cache_wback;           /**< Cache write back (WB) counter */
    
    /** Cache Memory Structures */
    cache_entry *vc_cache_mem;          /**< Pointer to the VCs memory blocks */
    
} vcache_core;

/** \brief Structure for a cache with optional victim cache (VC) */
typedef struct cache_core
{
    /** Cache Configuration Variables */
    unsigned char cache_level;      /**< Cache level if 1 means L1 cache */
    unsigned int cache_size;        /**< Cache size in bytes */
    unsigned int cache_assoc;       /**< Cache associativity or number of ways in cache */
    unsigned int cache_blksize;     /**< Cache block size */
    unsigned int cache_vc_blks;     /**< Number of blocks in victim cache */
    
    /** Cache Derived Variables */
    unsigned int cache_nsets;       /**< Number of sets in cache */
    unsigned int blkoffs_bitmask;   /**< Block offset bitmask */
    unsigned int ind_bitmask;       /**< Index bitmask */
    unsigned char ind_bitpos;       /**< Position of the index bits in address */
    unsigned int tag_bitmask;       /**< Tag bitmask */
    unsigned char tag_bitpos;       /**< Position of the tag bits in address */
    
    /** Cache Counter Variables */
    unsigned int cacheacc_read;     /**< Cache read access counter */
    unsigned int cacheacc_rmiss;    /**< Cache read miss counter */
    unsigned int cacheacc_write;    /**< Cache write access counter */
    unsigned int cacheacc_wmiss;    /**< Cache write miss counter */
    unsigned int cache_swapreq;     /**< Cache swap request counter. Not used if VC disabled */
    unsigned int cache_wback;       /**< Cache write back counter. Not used if VC enabled */
    
    /** Cache Memory Structures */
    cache_entry *cache_mem;             /**< Pointer to cache memory blocks */
    vcache_core *my_vcache;             /**< Pointer to my victim cache (VC) */
    struct cache_core *next_lvl_mem;    /**< Pointer to next cache in memory hierarchy */
    
} cache_core;

/* Clear dynamically allocated memory for the cache and its associated structures */
void free_alloc_mem(cache_core *mem_to_free);

/* Allocate memory for cache data memory */
cache_entry *init_cache_memblk(unsigned int cache_sets, unsigned int cache_assoc);

/* Clear and initialize cache data memory */
void clear_cache_blks(cache_entry *blks_to_clr, unsigned int cache_setno, unsigned int cache_assocno);

/* Allocate memory for victim cache and initialize it */
vcache_core *init_vcache(unsigned int vc_blks, unsigned int tag_bmask, unsigned char tag_bpos);

/* Allocate memory for a cache instance and initialize it */
cache_core *init_cache_level(unsigned char cache_lvl, unsigned int size, unsigned int blk_size, unsigned int assoc, unsigned int vc_blocks);

#endif
