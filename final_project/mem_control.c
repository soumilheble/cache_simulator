/**
 * \file mem_control.c
 * \author Soumil Krishnanand Heble
 * \date 09/29/2018
 * \brief Source: Cache core controller code for simulation
 */

#include "mem_control.h"
#include "mem_dump.h"
#include <stdio.h>
#include <stdlib.h>

#define READ_REQ    1
#define WRITE_REQ   0

cache_core *mem_hierarchy_array[2]; /** Array of pointers to the instantiated cache structure objects */

/**
 * \brief Update replacement policy rank of the blocks in the set recently accessed
 * \param[in] cache_lru_upd Pointer to cache memory block structure of the set recently accessed
 * \param[in] cache_assc Associativity of the cache level
 * \param[in] tag_toupd Tag bits of the block whose replacement policy rank is to be updated
 * \param[in] upd_index Position if the cache line to be updated in the set
 */
void replp_update(cache_entry *cache_lru_upd, unsigned int cache_assc, unsigned int tag_toupd, unsigned int upd_index)
{
    /** 0 is the rank for the MRU cache line and Associativity-1 is the rank of the LRU cache line */
    
    unsigned int loop_i;    /** Loop Counter */
    unsigned int alrdy_exists;
    
    if(upd_index==cache_assc)
    {
        alrdy_exists = 0xFF;
    }
    else
    {
        alrdy_exists = (cache_lru_upd+upd_index)->repp_rank;
    }
    
    /** Iterate over the entire set of the cache where the recently accessed block resides */
    for(loop_i=0; loop_i<cache_assc; loop_i++)
    {
        /** Only is the cache line is valid do */
        if((cache_lru_upd+loop_i)->block_valid)
        {
            /** If its the tag whose LRU is to be updated assign it the max rank */
            if(((cache_lru_upd+loop_i)->cache_tag)==tag_toupd)
            {
                (cache_lru_upd+loop_i)->repp_rank = 0;
            }
            else    /** Otherwise do */
            {
                if(alrdy_exists==0xFF) /* If the cache had a space left prior to the new block addition then do */
                {
                    /* DO saturation addition on the replacement policy rank until it hits the cache associativity - 1 */
                    ((cache_lru_upd+loop_i)->repp_rank)++;
                    if(((cache_lru_upd+loop_i)->repp_rank)==cache_assc)
                    {
                        ((cache_lru_upd+loop_i)->repp_rank)--;
                    }
                }
                else    /** Otherwise if the recently added block had a rank assigned do */ 
                {
                    /** Increment the rank of blocks whose rank is less than the previous rank of the recently accessed block */
                    if(((cache_lru_upd+loop_i)->repp_rank)<alrdy_exists)
                    {
                        ((cache_lru_upd+loop_i)->repp_rank)++;
                    }
                }
            }
        }
        else    /** otherwise its not valid, break out since cache lines filled in order and once a not valid block is found the following cache lines are empty as well */
        {
            break;
        }
    }
}

/**
 * \brief Create and initialize cache modules and set the memory hierarchy
 * \param[in] cache_blksize Block size in caches
 * \param[in] l1_size L1 cache data memory size
 * \param[in] l1_assoc L1 cache associativity
 * \param[in] l1_vc_blocks L1 victim cache number of blocks
 * \param[in] l2_size L2 cache data memory size
 * \param[in] l2_assoc L2 cache associativity
 */
void setup_mem_hierarchy(unsigned long int cache_blksize, unsigned long int l1_size, unsigned long int l1_assoc, 
                         unsigned long int l1_vc_blocks, unsigned long int l2_size, unsigned long int l2_assoc)
{
    /** Set up level 1 cache based on the cache specifications passed as argument */
    mem_hierarchy_array[0] = init_cache_level((unsigned char)1, (unsigned int)l1_size, (unsigned int)cache_blksize, (unsigned int)l1_assoc, (unsigned int)l1_vc_blocks);
    
    /** If level 2 cache specified then set it up as well and assign it as the next level to the level 1 cache */
    if(l2_size>0 && l2_assoc>0)
    {
        mem_hierarchy_array[1] = init_cache_level((unsigned char)2, (unsigned int)l2_size, (unsigned int)cache_blksize, (unsigned int)l2_assoc, (unsigned int)0);
        mem_hierarchy_array[0]->next_lvl_mem = mem_hierarchy_array[1];
    }
    else    /** Otherwise set the second level cache pointer to NULL */
    {
        mem_hierarchy_array[1] = NULL;
    }
}

/**
 * \brief Swap function for VC, takes care of writebacks to lower level
 * \param[in] ptrto_my_vc Pointer to VC structure object
 * \param[in] req_addr Address to search in VC
 * \param[in] vicblk_addr 32-bit address of victim block
 * \param[in] vicblk_ptr Pointer to the vicim cache block from master cache
 * \param[in] cache_nxtlvl Pointer to cache on the next level for processing writebacks
 * \return Pointer to the block returned (NULL if no block is being returned) Dynamically allocated block free after use
 */
cache_entry *swap_vc(vcache_core *ptrto_my_vc, unsigned int req_addr, unsigned int vicblk_addr, cache_entry *vicblk_ptr, cache_core *cache_nxtlvl)
{
    unsigned int vc_strip_tag = (req_addr & (ptrto_my_vc->tag_bitmask)) >> (ptrto_my_vc->tag_bitpos);   /** Strip the tag from the passed block address by using the bitmask and bitmask offset */
    
    cache_entry *temp_cache_blk = (cache_entry *)(ptrto_my_vc->vc_cache_mem);   /** Assign the VC cache data pointer to a pointer for easy manipulation */
    
    unsigned int loop_i;    /** Loop counter */
    unsigned char h_miss_flag = 0;  /** Hit/miss flag */
    
    /** LRU index and rank variable for tracking a cache line for eviction */
    unsigned int lru_ind = 0;
    unsigned int lru_rank = 0;
    
    /** Iterate over the entire set of VC (since VC is fully associative its all the blocks in the VC) */
    for(loop_i=0; loop_i<(ptrto_my_vc->vcache_blks); loop_i++)
    {
        /** Only if the cache line is valid do */
        if(((temp_cache_blk+loop_i)->block_valid)==1)
        {
            /** Compare the cache line tag and if hit do */
            if(((temp_cache_blk+loop_i)->cache_tag)==vc_strip_tag)
            {
                /** Set hit/miss tag and break out if the loop */
                h_miss_flag = 1;
                break;
            }
            else    /** Otherwise if tag does not match then note its rank and index (if less than the current rank) for picking a block for eviction */
            {
                if(((temp_cache_blk+loop_i)->repp_rank)>=lru_rank)
                {
                    lru_ind = loop_i;
                    lru_rank = ((temp_cache_blk+loop_i)->repp_rank);
                }
            }
        }
        else    /** Otherwise the cache line is not valid then the following lines are also not valid (because VC filled in order) so add the passed victim block and return NULL */
        {
            /** Copy the victim block to the empty cache line */
            /** Clear the index since only one set in VC */
            (temp_cache_blk+loop_i)->cache_index = 0;
            
            /** Strip the tag from the VC address and store along with the cache line data */
            (temp_cache_blk+loop_i)->cache_tag = (vicblk_addr & (ptrto_my_vc->tag_bitmask)) >> (ptrto_my_vc->tag_bitpos);
            
            (temp_cache_blk+loop_i)->block_valid = 1;
            (temp_cache_blk+loop_i)->block_dirty = (vicblk_ptr->block_dirty);
            
            /** Update the LRU of the victim block */
            replp_update(temp_cache_blk, ptrto_my_vc->vcache_blks, (temp_cache_blk+loop_i)->cache_tag, loop_i);
            
            return NULL;
        }
    }
    
    /** If its a hit then do */
    if(h_miss_flag==1)
    {
        /** Update swap success counter */
        (ptrto_my_vc->cache_swap_success)++;
        
        /** Malloc memory to store the cache line to be sent back */
        cache_entry *vic2_blk = (cache_entry *)malloc(sizeof(cache_entry));
        
        /** Copy the cache line hit to the temporary memory */
        vic2_blk->cache_index = 0;
        vic2_blk->cache_tag = (temp_cache_blk+loop_i)->cache_tag;
        vic2_blk->block_valid = 1;
        vic2_blk->block_dirty = (temp_cache_blk+loop_i)->block_dirty;
        vic2_blk->repp_rank = (temp_cache_blk+loop_i)->repp_rank;
        
        /** Copy the victim block in place of the hit cache line */
        /** Clear the index since only one set in VC */
        (temp_cache_blk+loop_i)->cache_index = 0;
        
        /** Strip the tag from the VC address and store along with the cache line data */
        (temp_cache_blk+loop_i)->cache_tag = (vicblk_addr & (ptrto_my_vc->tag_bitmask)) >> (ptrto_my_vc->tag_bitpos);
        
        (temp_cache_blk+loop_i)->block_valid = 1;
        (temp_cache_blk+loop_i)->block_dirty = (vicblk_ptr->block_dirty);
        
        /** Update the LRU of the victim block only if the cache line isn't already the MRU */
        if(((temp_cache_blk+loop_i)->repp_rank)!=0)
        {
            replp_update(temp_cache_blk, ptrto_my_vc->vcache_blks, (temp_cache_blk+loop_i)->cache_tag, loop_i);
        }
        
        /** Return the hit cache line to the parent cache */
        return vic2_blk;
    }
    else    /** Otherwise if its a miss then the cache is full so do */
    {
        /** If the victim block 2 picked for eviction is dirty do */
        if((temp_cache_blk+lru_ind)->block_dirty)
        {
            /** Update the VC writeback counter */
            (ptrto_my_vc->cache_wback)++;
            
            /** If there is a lower level cache do */
            if(cache_nxtlvl!=NULL)
            {
                unsigned recons_vic2_blkaddr = ((temp_cache_blk+lru_ind)->cache_tag)<<(ptrto_my_vc->tag_bitpos);
                
                /** Send a write request to the address of the victim block 2 to the lower level cache */
                process_addr_req(WRITE_REQ, recons_vic2_blkaddr, cache_nxtlvl);
            }
        }
        
        /** Clear the index since only one set in VC */
        (temp_cache_blk+lru_ind)->cache_index = 0;
        
        /** Strip the tag from the VC address and store along with the cache line data */
        (temp_cache_blk+lru_ind)->cache_tag = (vicblk_addr & (ptrto_my_vc->tag_bitmask)) >> (ptrto_my_vc->tag_bitpos);
        
        (temp_cache_blk+lru_ind)->block_valid = 1;
        (temp_cache_blk+lru_ind)->block_dirty = (vicblk_ptr->block_dirty);
        
        /** Update the LRU of the victim block */
        replp_update(temp_cache_blk, ptrto_my_vc->vcache_blks, (temp_cache_blk+lru_ind)->cache_tag, lru_ind);
        
        return NULL;
    }
}

/**
 * \brief Process a read/write request for an address sent to the memory hiererchy
 * \param[in] rw_req_flag Read/write access flag (1 - read, 0 - write)
 * \param[in] addr_to_proc 32-bit address accessed
 * \param[in] memlvl_req Pointer to level of memory where access is requested
 * \return Success/failure flag (0 - failure, 1 - success)
 */
unsigned char process_addr_req(unsigned char rw_req_flag, unsigned int addr_to_proc, cache_core *memlvl_req)
{
    /** If its a read request update read counter */
    if(rw_req_flag==READ_REQ)
    {
        (memlvl_req->cacheacc_read)++;
    }
    else if(rw_req_flag==WRITE_REQ) /** Otherwise if its a write request update write counter */
    {
        (memlvl_req->cacheacc_write)++;
    }
    
    unsigned int temp_strip_tag = (addr_to_proc&(memlvl_req->tag_bitmask))>>(memlvl_req->tag_bitpos);   /** Strip the tag from the address and apply the tag offset */
    unsigned int temp_strip_index = (addr_to_proc&(memlvl_req->ind_bitmask))>>(memlvl_req->ind_bitpos); /** Strip the index from the address and apply the index offset */
    
    unsigned int loop_i;    /** Loop counter */
    unsigned char h_miss_flag = 0;  /** Hit/miss flag */
    
    /** LRU index and rank variable for tracking a cache line for eviction */
    unsigned int lru_ind = 0;
    unsigned int lru_rank = 0;
    
    /** Assign the cache data pointer to a pointer for easy manipulation */
    cache_entry *temp_cache_blk = (cache_entry *)(memlvl_req->cache_mem);
    
    /** Offset the data pointer to the cache set where the requested address might be */
    temp_cache_blk = temp_cache_blk+(temp_strip_index*(memlvl_req->cache_assoc));
    
    /** Iterate over the entire set of the cache */
    for(loop_i=0; loop_i<(memlvl_req->cache_assoc); loop_i++)
    {
        /** Only if the cache line is valid do */
        if((temp_cache_blk+loop_i)->block_valid==1)
        {
            /** Compare the cache line tag and if hit do */
            if((temp_cache_blk+loop_i)->cache_tag==temp_strip_tag)
            {
                /** Set hit/miss tag and break out if the loop */
                h_miss_flag = 1;
                break;
            }
            else    /** Otherwise the tag does not match then note its rank and index (if less than the current rank) for picking a block for eviction */
            {
                if(((temp_cache_blk+loop_i)->repp_rank)>=lru_rank)
                {
                    lru_ind = loop_i;
                    lru_rank = (temp_cache_blk+loop_i)->repp_rank;
                }
            }
        }
        else    /** Otherwise the cache line not valid then the following lines are also not valid (because set filled in order) so do */
        {
            /** If it is a read request update read miss counter */
            if(rw_req_flag==READ_REQ)
            {
                (memlvl_req->cacheacc_rmiss)++;
            }
            else if(rw_req_flag==WRITE_REQ) /** Otherwise if its a write request update write miss counter */
            {
                (memlvl_req->cacheacc_wmiss)++;
            }
            
            /** If there is a next level cache send a read request with the required address */
            if((memlvl_req->next_lvl_mem)!=NULL)
            {
                process_addr_req(READ_REQ, addr_to_proc, memlvl_req->next_lvl_mem);
            }
            
            (temp_cache_blk+loop_i)->cache_index = temp_strip_index;    /** Store the block index in the cache line */
            (temp_cache_blk+loop_i)->cache_tag = temp_strip_tag;    /** Store the block tag in the cache line */
            (temp_cache_blk+loop_i)->block_valid = 1;   /** Set the valid bit of the cache line */
            (temp_cache_blk+loop_i)->block_dirty = 0;  /** Clear dirty bit */
            
            /** If it was a write request set the dirty bit */
            if(rw_req_flag==WRITE_REQ)
            {
                (temp_cache_blk+loop_i)->block_dirty = 1;
            }
            
            /** Update the LRU of the added block */
            replp_update(temp_cache_blk, memlvl_req->cache_assoc, (temp_cache_blk+loop_i)->cache_tag, loop_i);
            
            return 1;
        }
    }
    
    /** If hit then do */
    if(h_miss_flag==1)
    {
        /** If it was a write request set the dirty bit */
        if(rw_req_flag==WRITE_REQ)
        {
            (temp_cache_blk+loop_i)->block_dirty = 1;
        }
        
        /** Update the LRU of the accessed block only if the cache line isn't already the MRU */
        if(((temp_cache_blk+loop_i)->repp_rank)!=0)
        {
            replp_update(temp_cache_blk, memlvl_req->cache_assoc, (temp_cache_blk+loop_i)->cache_tag, loop_i);
        }
        
        return 1;
    }
    else    /** Otherwise its a miss then the cache is full so do */
    {
        /** If it is a read request update read miss counter */
        if(rw_req_flag==READ_REQ)
        {
            (memlvl_req->cacheacc_rmiss)++;
        }
        else if(rw_req_flag==WRITE_REQ) /**Otherwise if its a write request update write miss counter */
        {
            (memlvl_req->cacheacc_wmiss)++;
        }
        
        /** Reconstruct the block address of the victim block picked for eviction */
        unsigned int reconstructed_vic_addr = ((temp_cache_blk+lru_ind)->cache_tag)<<(memlvl_req->tag_bitpos);
        reconstructed_vic_addr = reconstructed_vic_addr|(((temp_cache_blk+lru_ind)->cache_index)<<(memlvl_req->ind_bitpos));
        
        /** If the cache has a victim cache then do */
        if((memlvl_req->cache_vc_blks)!=0)
        {
            /** Update the swap request counter */
            (memlvl_req->cache_swapreq)++;
            
            /** Pass the victim block to the victim cache to work its magic and store return value in a pointer */
            cache_entry *vcret_cache_blk = swap_vc(memlvl_req->my_vcache, addr_to_proc, reconstructed_vic_addr, (temp_cache_blk+lru_ind), memlvl_req->next_lvl_mem);
            
            /** If the victim cache returns a NULL pointer then do */
            if(vcret_cache_blk==NULL)
            {
                /** If there is a next level cache send a read request with the required address */
                if((memlvl_req->next_lvl_mem)!=NULL)
                {
                    process_addr_req(READ_REQ, addr_to_proc, memlvl_req->next_lvl_mem);
                }
                
                (temp_cache_blk+lru_ind)->cache_index = temp_strip_index;   /** Store the block index in the cache line */
                (temp_cache_blk+lru_ind)->cache_tag = temp_strip_tag;   /** Store the block tag in the cache line */
                (temp_cache_blk+lru_ind)->block_valid = 1;  /** Set the valid bit of the cache line */
                (temp_cache_blk+lru_ind)->block_dirty = 0;  /** Clear dirty bit */
                
                /** If it was a write request set the dirty bit */
                if(rw_req_flag==WRITE_REQ)
                {
                    (temp_cache_blk+lru_ind)->block_dirty = 1;
                }
                
                /** Update the LRU of the accessed block */
                replp_update(temp_cache_blk, memlvl_req->cache_assoc, (temp_cache_blk+lru_ind)->cache_tag, lru_ind);
                
                return 1;
            }
            else    /** Otherwise if the victim cache returns the found block then do */
            {
                (temp_cache_blk+lru_ind)->cache_index = temp_strip_index;   /** Set the returned block index in the cache line */
                (temp_cache_blk+lru_ind)->cache_tag = temp_strip_tag;   /** Set the returned block tag in the cache line */
                (temp_cache_blk+lru_ind)->block_dirty = vcret_cache_blk->block_dirty;   /** Copy the return blocks dirty bit to the cache line */
                (temp_cache_blk+lru_ind)->block_valid = 1;  /** Set the valid bit */
                
                /** If it was a write request set the dirty bit */
                if(rw_req_flag==WRITE_REQ)
                {
                    (temp_cache_blk+lru_ind)->block_dirty = 1;
                }
                
                /** Update the LRU of the accessed block */
                replp_update(temp_cache_blk, memlvl_req->cache_assoc, (temp_cache_blk+lru_ind)->cache_tag, lru_ind);
                
                free(vcret_cache_blk);  /** Free the memory allocated by swap_vc function */
                
                return 1;
            }
        }
        else    /** Otherwise there is to victim cache */
        {
            /** If the block picked for eviction is dirty then do */
            if(((temp_cache_blk+lru_ind)->block_dirty)==1)
            {
                /** Update the write back counter */
                (memlvl_req->cache_wback)++;
                
                /** If there is a next level cache send a write request with the victim block address */
                if((memlvl_req->next_lvl_mem)!=NULL)
                {
                    process_addr_req(WRITE_REQ, reconstructed_vic_addr, memlvl_req->next_lvl_mem);
                }
            }
            
            /** If there is a next level cache send a read request with the required address */
            if((memlvl_req->next_lvl_mem)!=NULL)
            {
                process_addr_req(READ_REQ, addr_to_proc, memlvl_req->next_lvl_mem);
            }
            
            (temp_cache_blk+lru_ind)->cache_index = temp_strip_index;   /** Store the block index in the cache line */
            (temp_cache_blk+lru_ind)->cache_tag = temp_strip_tag;   /** Store the block tag in the cache line */
            (temp_cache_blk+lru_ind)->block_valid = 1;  /** Set the valid bit of the cache line */
            (temp_cache_blk+lru_ind)->block_dirty = 0;  /** Clear dirty bit */
            
            /** If it was a write request set the dirty bit */
            if(rw_req_flag==WRITE_REQ)
            {
                (temp_cache_blk+lru_ind)->block_dirty = 1;
            }
            
            /** Update the LRU of the accessed block */
            replp_update(temp_cache_blk, memlvl_req->cache_assoc, (temp_cache_blk+lru_ind)->cache_tag, lru_ind);
            
            return 1;
        }
    }
}

/**
 * \brief Print simulation results and free dynamically allocated memory
 * \param[in] cache_specs Pointer to cache specification structure
 * \param[in] trace_fname Pointer to character array containing the trace file name
 */
void end_mem_hierarchy(cache_params *cache_specs, char *trace_fname)
{
    /** Print the cache specifications */
    dump_config(cache_specs, trace_fname);
    
    /** Print the cache contents of L1 cache as per project specifications */
    dump_cache_contents(mem_hierarchy_array[0]);
    
    /** If second level cache exists the do */
    if(mem_hierarchy_array[1]!=NULL)
    {
        /** Print the cache contents of L2 cache as per project specifications */
        dump_cache_contents(mem_hierarchy_array[1]);
    }
    
    /** Print the simulation results */
    dump_sim_result();
    
    /** Free the L1 cache memory allocated */
    free_alloc_mem(mem_hierarchy_array[0]);
    
    /** If second level cache exists the do */
    if(mem_hierarchy_array[1]!=NULL)
    {
        /** Free the L2 cache memory allocated */
        free_alloc_mem(mem_hierarchy_array[1]);
    }
}
