/**
 * \file sim_cache.h
 * \author Adith Vastrad
 * \date 09/27/2018
 * \brief Header: Boilerplate file for cache simulator project.
 */

#ifndef SIM_CACHE_H
#define SIM_CACHE_H

/** \brief Structure for program arguments passed */
typedef struct cache_params{
    unsigned long int block_size;       /**< Cache Block Size */
    unsigned long int l1_size;          /**< L1 Cache Size in bytes*/
    unsigned long int l1_assoc;         /**< L1 Cache Associativity */
    unsigned long int vc_num_blocks;    /**< Number of blocks in the Victim Cache */
    unsigned long int l2_size;          /**< L2 Cache Size in bytes*/
    unsigned long int l2_assoc;         /**< L2 Cache Associativity */
} cache_params;

// Put additional data structures here as per your requirement

#endif