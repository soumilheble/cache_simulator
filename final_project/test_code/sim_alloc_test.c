/**
 * \file sim_alloc_test.c
 * \author Soumil Krishnanand Heble
 * \date 09/28/2018
 * \brief Source: Cache construct memory allocate deallocate test.
 */

#include "gen_cache.h"
#include "mem_dump.h"
#include <stdio.h>

int main(void)
{
    cache_core *ptr_to_mh[2];                               /** Pointer to the cache constructs. */
    ptr_to_mh[0] = init_cache_level(1, 1024, 16, 2, 16);    /** L1 Cache with 1024 byte size, 16 bytes per block, 2 way and 16 blocks of VC. */
    ptr_to_mh[1] = init_cache_level(2, 8192, 16, 4, 0);    /** L2 Cache with 16384 byte size, 16 bytes per block, 4 way and no VC. */
    
    /** Print pointer addresses allocated to the data structures. */
    printf("L1 Cache Address: %p\n",ptr_to_mh[0]);
    printf("L1 Cache Blocks Address: %p\n",ptr_to_mh[0]->cache_mem);
    //printf("L1 Cache VC Address: %p\n",ptr_to_mh[0]->my_vcache);
    //printf("L1 Cache VC Blocks Address: %p\n",(ptr_to_mh[0]->my_vcache)->vc_cache_mem);
    printf("L2 Cache Address: %p\n",ptr_to_mh[1]);
    printf("L2 Cache Blocks Address: %p\n",ptr_to_mh[1]->cache_mem);
    
    printf("\n");
    /** Print L1 Cache Contents */
    printf("L1 Cache Level: %u\n", (unsigned int)ptr_to_mh[0]->cache_level);
    printf("L1 Cache Size: %u\n", ptr_to_mh[0]->cache_size);
    printf("L1 Cache Associativity: %u\n", ptr_to_mh[0]->cache_assoc);
    printf("L1 Cache Block Size: %u\n", ptr_to_mh[0]->cache_blksize);
    printf("L1 Cache VC Blocks: %u\n", ptr_to_mh[0]->cache_vc_blks);
    printf("L1 Cache Sets: %u\n", ptr_to_mh[0]->cache_nsets);
    printf("L1 Cache Block Offset Mask: %x\n", ptr_to_mh[0]->blkoffs_bitmask);
    printf("L1 Cache Index Mask: %x\n", ptr_to_mh[0]->ind_bitmask);
    printf("L1 Cache Index Pos: %u\n", (unsigned int)ptr_to_mh[0]->ind_bitpos);
    printf("L1 Cache Tag Mask: %x\n", ptr_to_mh[0]->tag_bitmask);
    printf("L1 Cache Tag Pos: %u\n", (unsigned int)ptr_to_mh[0]->tag_bitpos);
    printf("L1 Cache Read Counter: %u\n", ptr_to_mh[0]->cacheacc_read);
    printf("L1 Cache Read Miss Counter: %u\n", ptr_to_mh[0]->cacheacc_rmiss);
    printf("L1 Cache Write Counter: %u\n", ptr_to_mh[0]->cacheacc_write);
    printf("L1 Cache Write Miss Counter: %u\n", ptr_to_mh[0]->cacheacc_wmiss);
    printf("L1 Cache Swap Request Counter: %u\n", ptr_to_mh[0]->cache_swapreq);
    printf("L1 Cache Write Back Counter: %u\n", ptr_to_mh[0]->cache_wback);
    
    if(ptr_to_mh[0]->cache_vc_blks)
    {
        printf("VC Blocks: %u\n", (ptr_to_mh[0]->my_vcache)->vcache_blks);
        printf("VC Tag Bit Mask: %x\n", (ptr_to_mh[0]->my_vcache)->tag_bitmask);
        printf("VC Tag Bit Mask: %u\n", (unsigned int)(ptr_to_mh[0]->my_vcache)->tag_bitpos);
        printf("VC Swap Success Counter: %u\n", (ptr_to_mh[0]->my_vcache)->cache_swap_success);
        printf("VC Write Back Counter: %u\n", (ptr_to_mh[0]->my_vcache)->cache_wback);
    }
    
    printf("\n");
    /** Print L2 Cache Contents */
    printf("L2 Cache Level: %u\n", (unsigned int)ptr_to_mh[1]->cache_level);
    printf("L2 Cache Size: %u\n", ptr_to_mh[1]->cache_size);
    printf("L2 Cache Associativity: %u\n", ptr_to_mh[1]->cache_assoc);
    printf("L2 Cache Block Size: %u\n", ptr_to_mh[1]->cache_blksize);
    printf("L2 Cache VC Blocks: %u\n", ptr_to_mh[1]->cache_vc_blks);
    printf("L2 Cache Sets: %u\n", ptr_to_mh[1]->cache_nsets);
    printf("L2 Cache Block Offset Mask: %x\n", ptr_to_mh[1]->blkoffs_bitmask);
    printf("L2 Cache Index Mask: %x\n", ptr_to_mh[1]->ind_bitmask);
    printf("L2 Cache Index Pos: %u\n", (unsigned int)ptr_to_mh[1]->ind_bitpos);
    printf("L2 Cache Tag Mask: %x\n", ptr_to_mh[1]->tag_bitmask);
    printf("L2 Cache Tag Pos: %u\n", (unsigned int)ptr_to_mh[1]->tag_bitpos);
    printf("L2 Cache Read Counter: %u\n", ptr_to_mh[1]->cacheacc_read);
    printf("L2 Cache Read Miss Counter: %u\n", ptr_to_mh[1]->cacheacc_rmiss);
    printf("L2 Cache Write Counter: %u\n", ptr_to_mh[1]->cacheacc_write);
    printf("L2 Cache Write Miss Counter: %u\n", ptr_to_mh[1]->cacheacc_wmiss);
    printf("L2 Cache Swap Request Counter: %u\n", ptr_to_mh[1]->cache_swapreq);
    printf("L2 Cache Write Back Counter: %u\n", ptr_to_mh[1]->cache_wback);
    
    printf("\n");
    //dump_cache_contents(ptr_to_mh[0]);
    //dump_cache_contents(ptr_to_mh[1]);
    
    /** Free all dynamically allocated memory for L1 and L2 caches. */
    free_alloc_mem(ptr_to_mh[0]);
    free_alloc_mem(ptr_to_mh[1]);
    printf("Freed L1 & L2 Cache Allocated Dynamic Memory\n");
    
    return 0;
}
