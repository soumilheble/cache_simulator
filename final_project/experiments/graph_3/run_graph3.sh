#!/bin/bash

echo "Graph 3 Data Generator"

#Cache Default Parameters
BLKSIZE=32
L1SIZE=1024
L1WAYS=1
VCBLKS=0
L2SIZE=524288
L2WAYS=8

#Input and Output Files
ADDRTRACE="gcc_trace.txt"
OPFOLDER="./report_files"

#Loop Iteration Counter
RUNNUM=1

#Size Loop from 1kB to 256kB
while [ $L1SIZE -ne 524288 ]
do
	#Calculate the Associativity for a Fully Associative Cache
	FULLASSOC=$(( L1SIZE / BLKSIZE ))
	
	#Generate the Associativity Array
	L1WAYS=(1 2 4 8 $FULLASSOC)
	
	echo "Iteration: $RUNNUM"
	echo "Block Size: $BLKSIZE"
	echo "L1 Size: $L1SIZE"
	echo "L2 Size: $L2SIZE"
	echo "L2 Ways: $L2WAYS"
	
	#Associativity Loop
	for curr_assoc in "${L1WAYS[@]}"
	do
		echo "Associativity: $curr_assoc"
		
		#Generate Report File Name
		if [ $curr_assoc -eq $FULLASSOC ]
		then
			RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_FA.rpt"
		else
			RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_"$curr_assoc".rpt"
		fi

		#Print Command To Be Run
		echo "../../sim_cache $BLKSIZE $L1SIZE $curr_assoc $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE"
		
		#Run Simulator
		../../sim_cache $BLKSIZE $L1SIZE $curr_assoc $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE
	done
	echo $'\n'
	L1SIZE=$(( L1SIZE * 2 ))
	RUNNUM=$(( RUNNUM + 1 ))
done
