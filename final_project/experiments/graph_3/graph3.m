close all
clc

x_axis_log_l1size = [10
11
12
13
14
15
16
17
18];

y_axis_missrate_asc1 = [0.745902475
0.733427739
0.724152691
0.721586039
0.744157882
0.774374277
0.83229877
0.903950195
0.976894103];

y_axis_missrate_asc2 = [0.749466412
0.742416867
0.743808518
0.740070914
0.761826323
0.797889754
0.834796104
0.908553955
0.979011103];

y_axis_missrate_asc4 = [0.748195901
0.728906873
0.739117093
0.755129523
0.76890072
0.805479793
0.852442834
0.913776967
0.991181967];

y_axis_missrate_asc8 = [NaN
0.752154683
0.738588655
0.754198342
0.788611019
0.821642669
0.875372191
0.934732967
0.992421967];

y_axis_missrate_ascfa = [0.753766699
0.746684086
0.730135702
0.739888321
0.739030044
0.758082317
0.809570535
0.855982967
0.929505967];

figure(1);
hold on
plot(x_axis_log_l1size, y_axis_missrate_asc1, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_l1size, y_axis_missrate_asc2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_l1size, y_axis_missrate_asc4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_l1size, y_axis_missrate_asc8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_l1size, y_axis_missrate_ascfa, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
legend('Associativity: 1','Associativity: 2','Associativity: 4','Associativity: 8','Fully Associative');
title({'AAT vs. log_2(L1 Cache Size)','BLKSIZE=32, VC BLKS=0, L2 SIZE=512kB, L2 ASSOC=8'});
xlabel('log_2(L1 Cache Size)');
ylabel('Average Access Time (ns)');
grid on;
hold off;