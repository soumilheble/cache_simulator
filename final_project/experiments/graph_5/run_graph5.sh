#!/bin/bash

echo "Graph 5 Data Generator"

#Cache Default Parameters
BLKSIZE=32
L1SIZE=1024
L1WAYS=4
VCBLKS=0
L2SIZE=32768
L2WAYS=8

#Input and Output Files
ADDRTRACE="gcc_trace.txt"
OPFOLDER="./report_files"

#Loop Iteration Counter
RUNNUM=1

#L1 Size Loop from 1kB to 256kB
while [ $L1SIZE -ne 524288 ]
do
	echo "Iteration: $RUNNUM"
	echo "Block Size: $BLKSIZE"
	echo "L1 Size: $L1SIZE"
	echo "L1 Associativity: $L1WAYS"
	echo "L2 Associativity: $L2WAYS"
	
	#L2 Size loop from 32kB to 1MB
	while [ $L2SIZE -ne 2097152 ]
	do
		#Continue if L2 Size is <= L1 Size
		if [ "$L2SIZE" -le "$L1SIZE" ]
		then
			L2SIZE=$(( L2SIZE * 2))
			continue
		fi
		
		echo "L2 Size: $L2SIZE"
		
		#Generate Report File Name
		RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_"$L2SIZE".rpt"

		#Print Command To Be Run
		echo "../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE"
		
		#Run Simulator
		../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE

		#Update L2SIZE
		L2SIZE=$(( L2SIZE * 2 ))
	done
	echo $'\n'
	L1SIZE=$(( L1SIZE * 2 ))
	RUNNUM=$(( RUNNUM + 1 ))
	
	#Reset L2SIZE
	L2SIZE=32768
done
