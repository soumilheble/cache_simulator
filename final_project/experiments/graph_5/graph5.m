close all
clc

x_axis_log_l1size = [10
11
12
13
14
15
16
17
18];

y_axis_aat_l2size32k = [0.7157522
0.709742294
0.730434828
0.751446643
0.769178739
NaN
NaN
NaN
NaN];

y_axis_aat_l2size64k = [0.715815273
0.707657833
0.726969733
0.747194178
0.765038789
0.802460847
NaN
NaN
NaN];

y_axis_aat_l2size128k = [0.72294642
0.711885149
0.728518327
0.74760953
0.76389329
0.80080855
0.847860062
NaN
NaN];

y_axis_aat_l2size256k = [0.731178641
0.717434831
0.731973899
0.750061313
0.765525889
0.80233154
0.849354208
0.910700265
NaN];

y_axis_aat_l2size512k = [0.748195901
0.728906873
0.739117093
0.755129523
0.76890072
0.805479793
0.852442834
0.913776967
0.991181967];

y_axis_aat_l2size1M = [0.766410414
0.741186034
0.746762849
0.760554308
0.772512989
0.808849542
0.855748762
0.91707013
0.99447513];

figure(1);
hold on
plot(x_axis_log_l1size, y_axis_aat_l2size32k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_l1size, y_axis_aat_l2size64k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_l1size, y_axis_aat_l2size128k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_l1size, y_axis_aat_l2size256k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_l1size, y_axis_aat_l2size512k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_log_l1size, y_axis_aat_l2size1M, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
legend('L2 Size: 32kB','L2 Size: 64kB','L2 Size: 128kB','L2 Size: 256kB', 'L2 Size: 512kB', 'L2 Size: 1MB', 'Location', 'northwest');
title({'AAT vs. log_2(L1 Cache Size)','BLOCKSIZE=32, L1 ASSOC=4, VC BLKS=0, L2 ASSOC=8'});
xlabel('log_2(L1 Cache Size)');
ylabel('Average Access Time (ns)');
grid on;
hold off;