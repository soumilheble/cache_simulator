#!/bin/bash

echo "Graph 6 Data Generator"

#Cache Default Parameters
BLKSIZE=32
L1SIZE=1024
L1WAYS=1
VCBLKS=( 0 2 4 8 16 )
L2SIZE=65536
L2WAYS=8

#Input and Output Files
ADDRTRACE="gcc_trace.txt"
OPFOLDER="./report_files"

#Loop Iteration Counter
RUNNUM=1

#L1 Size Loop from 1kB to 32kB
while [ $L1SIZE -ne 65536 ]
do
	echo "Iteration: $RUNNUM"
	echo "Block Size: $BLKSIZE"
	echo "L1 Size: $L1SIZE"
	echo "L2 Size: $L2SIZE"
	echo "L2 Associativity: $L2WAYS"
	
	#Associativity Loop from 1 to 4
	while [ $L1WAYS -ne 8 ]
	do
		echo "L1 Associativity: $L1WAYS"
		for curr_vc in "${VCBLKS[@]}"
		do
			if [ $L1WAYS -gt 1 ]
			then
				if [ $curr_vc -gt 0 ]
				then
					break
				fi
			fi
		
			#Generate Report File Name
			RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_"$L1WAYS"_"$curr_vc".rpt"

			#Print Command To Be Run
			echo "../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $curr_vc $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE"
		
			#Run Simulator
			../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $curr_vc $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE
		done

		#Update L1WAYS
		L1WAYS=$(( L1WAYS * 2 ))
	done
	echo $'\n'
	L1SIZE=$(( L1SIZE * 2 ))
	RUNNUM=$(( RUNNUM + 1 ))
	
	#Reset L1WAYS
	L1WAYS=1
done
