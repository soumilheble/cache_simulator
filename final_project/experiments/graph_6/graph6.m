close all
clc

x_axis_log_l1size = [10
11
12
13
14
15];

y_axis_aat_avc10 = [0.701216746
0.699615664
0.701818713
0.707056151
0.734809078
0.767714044];

y_axis_aat_avc12 = [0.715383926
0.71100311
0.710679925
0.713807656
0.740208997
0.769295478];

y_axis_aat_avc14 = [0.710909004
0.707434303
0.708963304
0.712487232
0.738971616
0.76912732];

y_axis_aat_avc18 = [0.706557357
0.704713746
0.706757709
0.710938997
0.738772048
0.769744213];

y_axis_aat_avc116 = [0.701022141
0.70123871
0.704444198
0.710059702
0.737972443
0.769266187];

y_axis_aat_avc20 = [0.713754268
0.718329648
0.727327306
0.731049196
0.756398583
0.793496486];

y_axis_aat_avc40 = [0.715815273
0.707657833
0.726969733
0.747194178
0.765038789
0.802460847];

figure(1);
hold on
plot(x_axis_log_l1size, y_axis_aat_avc10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_l1size, y_axis_aat_avc12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_l1size, y_axis_aat_avc14, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_l1size, y_axis_aat_avc18, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_l1size, y_axis_aat_avc116, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_log_l1size, y_axis_aat_avc20, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
plot(x_axis_log_l1size, y_axis_aat_avc40, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.3010, 0.7450, 0.9330]);
legend('Assoc:1 VC Blks: 0','Assoc:1 VC Blks: 2','Assoc:1 VC Blks: 4','Assoc:1 VC Blks: 8', 'Assoc:1 VC Blks: 16', 'Assoc:2 VC Blks: 0', 'Assoc:4 VC Blks: 0', 'Location', 'northwest');
title({'Average Access Time vs. log_2(L1 Cache Size)','BLOCKSIZE=32B, L2 SIZE=64kB, L2 ASSOC=8'});
xlabel('log_2(L1 Cache Size)');
ylabel('Average Access Time (ns)');
grid on;
hold off;