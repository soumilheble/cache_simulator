close all
clc

x_axis_log_blksize = [4
5
6
7];

y_axis_missrate_l1size1k = [0.1473
0.1427
0.1584
0.2036];

y_axis_missrate_l1size2k = [0.1062
0.0962
0.1033
0.1334];

y_axis_missrate_l1size4k = [0.0755
0.0599
0.0619
0.083];

y_axis_missrate_l1size8k = [0.0595
0.0425
0.0386
0.0483];

y_axis_missrate_l1size16k = [0.0482
0.0283
0.0204
0.0198];

y_axis_missrate_l1size32k = [0.0475
0.0264
0.0156
0.0111];

figure(1);
hold on
plot(x_axis_log_blksize, y_axis_missrate_l1size1k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_blksize, y_axis_missrate_l1size2k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_blksize, y_axis_missrate_l1size4k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_blksize, y_axis_missrate_l1size8k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_blksize, y_axis_missrate_l1size16k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_log_blksize, y_axis_missrate_l1size32k, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
legend('L1 Size: 1kB','L1 Size: 2kB','L1 Size: 4kB','L1 Size: 8kB', 'L1 Size: 16kB', 'L1 Size: 32kB', 'Location', 'northwest');
title({'L1 Cache Miss Rate vs. log_2(Block Size)','L1 ASSOC=4, VC BLKS=0, L2 SIZE=0, L2 ASSOC=0'});
xlabel('log_2(Block Size)');
ylabel('L1 Cache Miss Rate');
grid on;
hold off;