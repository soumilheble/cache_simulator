#!/bin/bash

echo "Graph 4 Data Generator"

#Cache Default Parameters
BLKSIZE=16
L1SIZE=1024
L1WAYS=4
VCBLKS=0
L2SIZE=0
L2WAYS=0

#Input and Output Files
ADDRTRACE="gcc_trace.txt"
OPFOLDER="./report_files"

#Loop Iteration Counter
RUNNUM=1

#Blocksize Loop from 16 bytes to 128 bytes
while [ $BLKSIZE -ne 256 ]
do
	echo "Iteration: $RUNNUM"
	echo "Block Size: $BLKSIZE"
	echo "L1 Associativity: $L1WAYS"
	
	#Size Loop from 1kB to 32kB
	while [ $L1SIZE -ne 65536 ]
	do
		echo "L1 Size: $L1SIZE"
		
		#Generate Report File Name
		RPTFILE=$OPFOLDER"/"$RUNNUM"_"$BLKSIZE"_"$L1SIZE".rpt"

		#Print Command To Be Run
		echo "../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE"
		
		#Run Simulator
		../../sim_cache $BLKSIZE $L1SIZE $L1WAYS $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE

		#Update L1SIZE
		L1SIZE=$(( L1SIZE * 2 ))
	done
	echo $'\n'
	BLKSIZE=$(( BLKSIZE * 2 ))
	RUNNUM=$(( RUNNUM + 1 ))
	
	#Reset L1SIZE
	L1SIZE=1024
done
