close all
clc

x_axis_log_l1size = [10
11
12
13
14
15
16
17
18
19
20];

y_axis_missrate_asc1 = [4.004147
3.09786
2.161025
1.51053
1.125027
0.991123
0.955917
1.01603
0.962392
1.082031
1.21796];

y_axis_missrate_asc2 = [3.275929
2.314401
1.694661
1.144925
0.903297
0.841326
0.845437
0.895193
0.964509
1.086324
1.224626];

y_axis_missrate_asc4 = [3.01509
2.088116
1.389675
1.065423
0.802766
0.80189
0.840071
0.89886
0.976265
1.082998
1.218187];

y_axis_missrate_asc8 = [NaN
2.003756
1.266425
1.006861
0.811124
0.815131
0.861803
0.919816
0.977505
1.096757
1.224399];

y_axis_missrate_ascfa = [2.909184
1.957375
1.177898
0.984491
0.734238
0.75136
0.794861
0.841066
0.914589
0.994308
1.107054];

figure(1);
hold on
plot(x_axis_log_l1size, y_axis_missrate_asc1, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_l1size, y_axis_missrate_asc2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_l1size, y_axis_missrate_asc4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_l1size, y_axis_missrate_asc8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_l1size, y_axis_missrate_ascfa, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
legend('Associativity: 1','Associativity: 2','Associativity: 4','Associativity: 8','Fully Associative');
title({'AAT vs. log_2(L1 Cache Size)','BLKSIZE=32, VC BLKS=0, L2 SIZE=0, L2 ASSOC=0'});
xlabel('log_2(L1 Cache Size)');
ylabel('Average Access Time (ns)');
grid on;
hold off;