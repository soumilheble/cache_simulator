#!/bin/bash

echo "Graph 1 & 2 Data Generator"

#Cache Default Parameters
BLKSIZE=32
L1SIZE=1024
L1WAYS=1
VCBLKS=0
L2SIZE=0
L2WAYS=0

#Input and Output Files
ADDRTRACE="gcc_trace.txt"
OPFOLDER="./report_files"

#Loop Iteration Counter
RUNNUM=1

#Size Loop from 1kB to 1MB
while [ $L1SIZE -ne 2097152 ]
do
	#Calculate the Associativity for a Fully Associative Cache
	FULLASSOC=$(( L1SIZE / BLKSIZE ))
	
	#Generate the Associativity Array
	L1WAYS=(1 2 4 8 $FULLASSOC)
	
	echo "Iteration: $RUNNUM"
	echo "Block Size: $BLKSIZE"
	echo "L1 Size: $L1SIZE"
	
	#Associativity Loop
	for curr_assoc in "${L1WAYS[@]}"
	do
		echo "L1 Associativity: $curr_assoc"
		
		#Generate Report File Name
		if [ $curr_assoc -eq $FULLASSOC ]
		then
			RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_FA.rpt"
		else
			RPTFILE=$OPFOLDER"/"$RUNNUM"_"$L1SIZE"_"$curr_assoc".rpt"
		fi

		#Print Command To Be Run
		echo "../../sim_cache $BLKSIZE $L1SIZE $curr_assoc $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE"
		
		#Run Simulator
		../../sim_cache $BLKSIZE $L1SIZE $curr_assoc $VCBLKS $L2SIZE $L2WAYS $ADDRTRACE > $RPTFILE
	done
	echo $'\n'
	L1SIZE=$(( L1SIZE * 2 ))
	RUNNUM=$(( RUNNUM + 1 ))
done
