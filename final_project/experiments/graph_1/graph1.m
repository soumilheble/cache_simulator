close all
clc

x_axis_log_l1size = [10
11
12
13
14
15
16
17
18
19
20];

y_axis_missrate_asc1 = [0.1935
0.1477
0.1002
0.067
0.0461
0.0377
0.0329
0.0323
0.0258
0.0258
0.0258];

y_axis_missrate_asc2 = [0.156
0.1071
0.0753
0.0473
0.0338
0.0288
0.0271
0.0259
0.0258
0.0258
0.0258];

y_axis_missrate_asc4 = [0.1427
0.0962
0.0599
0.0425
0.0283
0.0264
0.0259
0.0258
0.0258
0.0258
0.0258];

y_axis_missrate_asc8 = [0.1363
0.0907
0.0536
0.0395
0.0277
0.0262
0.0259
0.0258
0.0258
0.0258
0.0258];

y_axis_missrate_ascfa = [0.137
0.0886
0.0495
0.0391
0.0263
0.0262
0.0258
0.0258
0.0258
0.0258
0.0258];

figure(1);
hold on
plot(x_axis_log_l1size, y_axis_missrate_asc1, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_log_l1size, y_axis_missrate_asc2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_log_l1size, y_axis_missrate_asc4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_log_l1size, y_axis_missrate_asc8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_log_l1size, y_axis_missrate_ascfa, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
legend('Associativity: 1','Associativity: 2','Associativity: 4','Associativity: 8','Fully Associative');
title({'L1 Cache Miss Rate vs. log_2(L1 Cache Size)','BLKSIZE=32, VC BLKS=0, L2 SIZE=0, L2 ASSOC=0'});
xlabel('log_2(L1 Cache Size)');
ylabel('L1 Cache Miss Rate');
hold off;
grid on;