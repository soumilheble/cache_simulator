/**
 * \file mem_dump.h
 * \author Soumil Krishnanand Heble
 * \date 09/28/2018
 * \brief Header: Dump memory hierarchy contents of cache memory
 */

#ifndef __MEM_DUMP_H__
#define __MEM_DUMP_H__

#include "sim_cache.h"
#include "gen_cache.h"

/* Prints out cache parameters as per project specifications and format */
void dump_config(cache_params *cache_specs, char *trace_fname);

/* Prints out cache contents as per project specifications and format */
void dump_cache_contents(cache_core *cache_core_dump);

/* Prints out simulation results as per project specifications and format */
void dump_sim_result();

#endif
