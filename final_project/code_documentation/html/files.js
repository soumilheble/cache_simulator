var files =
[
    [ "gen_cache.c", "gen__cache_8c.html", "gen__cache_8c" ],
    [ "gen_cache.h", "gen__cache_8h.html", "gen__cache_8h" ],
    [ "mem_control.c", "mem__control_8c.html", "mem__control_8c" ],
    [ "mem_control.h", "mem__control_8h.html", "mem__control_8h" ],
    [ "mem_dump.c", "mem__dump_8c.html", "mem__dump_8c" ],
    [ "mem_dump.h", "mem__dump_8h.html", "mem__dump_8h" ],
    [ "sim_cache.c", "sim__cache_8c.html", "sim__cache_8c" ],
    [ "sim_cache.h", "sim__cache_8h.html", [
      [ "cache_params", "structcache__params.html", "structcache__params" ]
    ] ]
];