var indexSectionsWithContent =
{
  0: "bcdefgilmnprstv",
  1: "cv",
  2: "gms",
  3: "cdefimprs",
  4: "bcilmnrstv",
  5: "ls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros"
};

