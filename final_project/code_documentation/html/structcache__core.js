var structcache__core =
[
    [ "blkoffs_bitmask", "structcache__core.html#aad117fcbcdd8a7e884066782532cec4f", null ],
    [ "cache_assoc", "structcache__core.html#a4a79e73c3b966201ca6b0ae0a367afd0", null ],
    [ "cache_blksize", "structcache__core.html#a6ed4fecb78c753421d91ddac0446c271", null ],
    [ "cache_level", "structcache__core.html#abcd9837e501e07ffdb058357870ca4ba", null ],
    [ "cache_mem", "structcache__core.html#ab47c0f90a0484f8b5cf5717d1a561733", null ],
    [ "cache_nsets", "structcache__core.html#a6ef4dd8a169ed078f799e885fc3fb3a0", null ],
    [ "cache_size", "structcache__core.html#acc064f9a11fe20896aaba257fc59e8d8", null ],
    [ "cache_swapreq", "structcache__core.html#a40e45e6c1b44d5853df18dbe489b4874", null ],
    [ "cache_vc_blks", "structcache__core.html#ad0cc87b84a5350e35e95fbfa684abf47", null ],
    [ "cache_wback", "structcache__core.html#aff2d726c2546a295e264461db94b7a75", null ],
    [ "cacheacc_read", "structcache__core.html#ac8add5d6a6dd77a5ab8a0311d4e80b5a", null ],
    [ "cacheacc_rmiss", "structcache__core.html#ad4ca3d55d1992bee75be8362557c4ac8", null ],
    [ "cacheacc_wmiss", "structcache__core.html#a1aef7d7559549d6e0297b4cea35e6928", null ],
    [ "cacheacc_write", "structcache__core.html#a1ce649543818434ef6388512d7e552bf", null ],
    [ "ind_bitmask", "structcache__core.html#aa080dfd58240c975fe9d1dba91510a8b", null ],
    [ "ind_bitpos", "structcache__core.html#a6d33c6afcd9c17f083042f19d457e7f2", null ],
    [ "my_vcache", "structcache__core.html#a84cdea4911c21ab3aa46953142643fd6", null ],
    [ "next_lvl_mem", "structcache__core.html#a2d317e642999432f3144965d4092435e", null ],
    [ "tag_bitmask", "structcache__core.html#a69ffc758da164a3e6dfeee9650cecf63", null ],
    [ "tag_bitpos", "structcache__core.html#a0eacd5c108ac7c8862cf81909a4579c2", null ]
];