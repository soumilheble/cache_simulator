/**
 * \file mem_dump.c
 * \author Soumil Krishnanand Heble
 * \date 09/28/2018
 * \brief Source: Dump memory hierarchy contents of cache memory
 */

#include "mem_dump.h"
#include <stdio.h>

/** sim_results_int array index-definition map */
#define L1_READ                  0  /** L1 cache reads */
#define L1_READ_MISS             1  /** L1 cache read misses */
#define L1_WRITE                 2  /** L1 cache writes */
#define L1_WRITE_MISS            3  /** L1 cache write misses */
#define L1_SWAP_REQ              4  /** L1 cache swap requests */
#define VC_L1_SWAP               5  /** Victim cache L1 swap requests */
#define L1_VC_WBACK              6  /** Victim cache L1 write backs */
#define L2_READ                  7  /** L2 cache reads */
#define L2_READ_MISS             8  /** L2 cache read misses */
#define L2_WRITE                 9  /** L2 cache writes */
#define L2_WRITE_MISS           10  /** L2 cache write misses */
#define L2_WBACK                11  /** L2 write backs */
#define TOTAL_MEM_TRAFFIC       12  /** Total Memory Traffic => If L1 = L1 Read Misses + L1 Write Misses - L1_VC Swaps + L1_VC Write Backs. If L2 = L2 Read Misses + L2 Write Misses + L2 Write Backs */
#define MAX_COUNT_INT           13  /** Number of elements in the integer result array */

/** sim_results_float array index-definition map. */
#define SWAP_REQ_RATE       0   /** Swap Request Rate = L1 Swap Requests / (L1 Reads + L1 Writes) */
#define L1_VC_MISS_RATE     1   /** L1 VC Miss Rate = L1 Read Misses + L1 Write Misses - L1_VC Swaps / (L1 Reads + L1 Writes) */
#define L2_MISS_RATE        2   /** L2 Miss Rate = L2 Read Misses + L2 Write Misses / (L2 Reads + L2 Writes) */
#define MAX_COUNT_FLOAT     3   /** Number of elements in the float result array */

unsigned int current_lru;   /** LRU index to print. */
float sim_results_float[MAX_COUNT_FLOAT];             /** Simulation results float array */
unsigned int sim_results_int[MAX_COUNT_INT];     /** Simulation results integer array */

/**
 * \brief Prints out cache parameters as per project specifications and format
 * \param[in] cache_specs Pointer to cache specification structure
 * \param[in] trace_fname Pointer to character array containing the trace file name
 */
void dump_config(cache_params *cache_specs, char *trace_fname)
{
    printf("===== Simulator configuration =====\n"
            "  BLOCKSIZE:%20lu\n"
            "  L1_SIZE:%22lu\n"
            "  L1_ASSOC:%21lu\n"
            "  VC_NUM_BLOCKS:%16lu\n"
            "  L2_SIZE:%22lu\n"
            "  L2_ASSOC:%21lu\n"
            "  trace_file:%19s\n\n", cache_specs->block_size, cache_specs->l1_size, cache_specs->l1_assoc, cache_specs->vc_num_blocks, cache_specs->l2_size, cache_specs->l2_assoc, trace_fname);
}

/**
 * \brief Prints out cache contents as per project specifications and format
 * \param[in] cache_core_dump Pointer to cache data structure
 */
void dump_cache_contents(cache_core *cache_core_dump)
{
    if(cache_core_dump->cache_level==1) /** If L1 cache instance is passed update its counters into results array */
    {
        sim_results_int[L1_READ] = cache_core_dump->cacheacc_read;
        sim_results_int[L1_READ_MISS] = cache_core_dump->cacheacc_rmiss;
        sim_results_int[L1_WRITE] = cache_core_dump->cacheacc_write;
        sim_results_int[L1_WRITE_MISS] = cache_core_dump->cacheacc_wmiss;
        
        if(cache_core_dump->cache_vc_blks)  /** If L1 cache instance has a VC then update VC counters into results array */
        {
            sim_results_int[L1_SWAP_REQ] = cache_core_dump->cache_swapreq;
            sim_results_int[L1_VC_WBACK] = (cache_core_dump->my_vcache)->cache_wback;
            sim_results_int[VC_L1_SWAP] = (cache_core_dump->my_vcache)->cache_swap_success;
        }
        else    /** If L1 cache instance does not have a VC then update L1 counters into results array and clear VC related counters in the results array */
        {
            sim_results_int[L1_SWAP_REQ] = 0;
            sim_results_int[L1_VC_WBACK] = cache_core_dump->cache_wback;
            sim_results_int[VC_L1_SWAP] = 0;
        }
        
        /** Calculate and update total memory counter in results array based on only L1 cache and VC, if present */
        sim_results_int[TOTAL_MEM_TRAFFIC] = sim_results_int[L1_READ_MISS]+sim_results_int[L1_WRITE_MISS]-sim_results_int[VC_L1_SWAP]+sim_results_int[L1_VC_WBACK];
        
        /** Calculate and update swap request rate in results array */
        sim_results_float[SWAP_REQ_RATE] = (float)sim_results_int[L1_SWAP_REQ]/(float)(sim_results_int[L1_READ]+sim_results_int[L1_WRITE]);
        
        /** Calculate and update L1_VC miss rate in results array */
        sim_results_float[L1_VC_MISS_RATE] = (float)(sim_results_int[L1_READ_MISS]+sim_results_int[L1_WRITE_MISS]-sim_results_int[VC_L1_SWAP])/(float)(sim_results_int[L1_READ]+sim_results_int[L1_WRITE]);
    }
    else if(cache_core_dump->cache_level==2)    /** If L1 cache instance is passed update its counters into results array */
    {
        sim_results_int[L2_READ] = cache_core_dump->cacheacc_read;
        sim_results_int[L2_READ_MISS] = cache_core_dump->cacheacc_rmiss;
        sim_results_int[L2_WRITE] = cache_core_dump->cacheacc_write;
        sim_results_int[L2_WRITE_MISS] = cache_core_dump->cacheacc_wmiss;
        sim_results_int[L2_WBACK] = cache_core_dump->cache_wback;
        
        /** Calculate and update total memory counter in results array based L2 cache */
        sim_results_int[TOTAL_MEM_TRAFFIC] = sim_results_int[L2_READ_MISS]+sim_results_int[L2_WRITE_MISS]+sim_results_int[L2_WBACK];
        
        /** Calculate and update L2 miss rate in results array */
        sim_results_float[L2_MISS_RATE] = (float)sim_results_int[L2_READ_MISS]/(float)(sim_results_int[L2_READ]);
    }
    
    /** Print out the cache contents based on the required formatting and MRU first */
    printf("===== L%d contents =====\n", cache_core_dump->cache_level);
    
    unsigned int set_index = 0;
    unsigned int assoc_index = 0;
    
    cache_entry *temp_cache_entry = (cache_entry *)cache_core_dump->cache_mem;
    
    for(set_index = 0; set_index<cache_core_dump->cache_nsets; set_index++)
    {
        current_lru = 0;
        printf("  set%4u:   ", set_index);
        
        while(current_lru<(cache_core_dump->cache_assoc))
        {
            for(assoc_index = 0; assoc_index<cache_core_dump->cache_assoc; assoc_index++)
            {
                if(((temp_cache_entry+(set_index*cache_core_dump->cache_assoc)+assoc_index)->block_valid)==1)
                {
                    if(((temp_cache_entry+(set_index*cache_core_dump->cache_assoc)+assoc_index)->repp_rank)==current_lru)
                    {
                        printf("%x ", (temp_cache_entry+(set_index*cache_core_dump->cache_assoc)+assoc_index)->cache_tag);
                        
                        if((temp_cache_entry+(set_index*cache_core_dump->cache_assoc)+assoc_index)->block_dirty)
                        {
                            printf("D  ");
                        }
                        else
                        {
                            printf("   ");
                        }
                        break;
                    }
                }
            }
            current_lru++;
        }
        printf("\n");
    }
    printf("\n");
    
    if(cache_core_dump->cache_vc_blks)  /** If VC is present then print out the cache contents based on the required formatting and MRU first */
    {
        printf("===== VC contents =====\n");
        
        temp_cache_entry = (cache_entry *)(cache_core_dump->my_vcache)->vc_cache_mem;
        
        for(set_index = 0; set_index<1; set_index++)
        {
            current_lru = 0;
            printf("  set%4u:  ", set_index);
            
            while(current_lru<cache_core_dump->cache_vc_blks)
            {
                for(assoc_index = 0; assoc_index<cache_core_dump->cache_vc_blks; assoc_index++)
                {
                    if(((temp_cache_entry+assoc_index)->block_valid)==1)
                    {
                        if(((temp_cache_entry+assoc_index)->repp_rank)==current_lru)
                        {
                            printf("%x ", (temp_cache_entry+assoc_index)->cache_tag);
                            
                            if((temp_cache_entry+assoc_index)->block_dirty)
                            {
                                printf("D ");
                            }
                            else
                            {
                                printf("  ");
                            }
                            break;
                        }
                    }
                }
                current_lru++;
            }
            printf("\n");
        }
        printf("\n");
    }
}

/**
 * \brief Prints out simulation results as per project specifications and format
 */
void dump_sim_result()
{
    printf("===== Simulation results =====\n");
    printf("  a. number of L1 reads:%28u\n", sim_results_int[L1_READ]);
    printf("  b. number of L1 read misses:%22u\n", sim_results_int[L1_READ_MISS]);
    printf("  c. number of L1 writes:%27u\n", sim_results_int[L1_WRITE]);
    printf("  d. number of L1 write misses:%21u\n", sim_results_int[L1_WRITE_MISS]);
    printf("  e. number of swap requests:%23u\n", sim_results_int[L1_SWAP_REQ]);
    printf("  f. swap request rate:%29.4f\n", sim_results_float[SWAP_REQ_RATE]);
    printf("  g. number of swaps:%31u\n", sim_results_int[VC_L1_SWAP]);
    printf("  h. combined L1+VC miss rate:%22.4f\n", sim_results_float[L1_VC_MISS_RATE]);
    printf("  i. number writebacks from L1/VC:%18u\n", sim_results_int[L1_VC_WBACK]);
    printf("  j. number of L2 reads:%28u\n", sim_results_int[L2_READ]);
    printf("  k. number of L2 read misses:%22u\n", sim_results_int[L2_READ_MISS]);
    printf("  l. number of L2 writes:%27u\n", sim_results_int[L2_WRITE]);
    printf("  m. number of L2 write misses:%21u\n", sim_results_int[L2_WRITE_MISS]);
    printf("  n. L2 miss rate:%34.4f\n", sim_results_float[L2_MISS_RATE]);
    printf("  o. number of writebacks from L2:%18u\n", sim_results_int[L2_WBACK]);
    printf("  p. total memory traffic:%26u\n", sim_results_int[TOTAL_MEM_TRAFFIC]);
}
