/**
 * \file mem_control.h
 * \author Soumil Krishnanand Heble
 * \date 09/29/2018
 * \brief Header: Cache core controller code for simulation
 */

#ifndef __MEM_CONTROL_H__
#define __MEM_CONTROL_H__

#include "gen_cache.h"
#include "sim_cache.h"

extern cache_core *mem_hierarchy_array[2];  /** Array of pointers to the instantiated cache structure objects */

/** Update replacement policy rank of the blocks in the set recently accessed */
void replp_update(cache_entry *cache_lru_upd, unsigned int cache_assc, unsigned int tag_toupd, unsigned int upd_index);

/** Create and initialize cache modules and set the memory hierarchy */
void setup_mem_hierarchy(unsigned long int cache_blksize, unsigned long int l1_size, unsigned long int l1_assoc, 
                         unsigned long int l1_vc_blocks, unsigned long int l2_size, unsigned long int l2_assoc);

/** Swap function for VC, takes care of writebacks to lower level */
cache_entry *swap_vc(vcache_core *ptrto_my_vc, unsigned int req_addr, unsigned int vicblk_addr, cache_entry *vicblk_ptr, cache_core *cache_nxtlvl);

/** Process a read/write request for an address sent to the memory hiererchy */
unsigned char process_addr_req(unsigned char rw_req_flag, unsigned int addr_to_proc, cache_core *memlvl_req);

/** Print simulation results and free dynamically allocated memory */
void end_mem_hierarchy(cache_params *cache_specs, char *trace_fname);

#endif
